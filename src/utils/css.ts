/**
 * Replaces style with `prop` prop name
 * with the `newProp` name.
 * Filters out all other styles.
 */
export function replaceProp(
  styles: React.CSSProperties,
  prop: string,
  newProp: string
) {
  return Object.entries(styles).reduce(
    (acc: Record<string, unknown>, [key, value]) => {
      if (key === prop) {
        acc[newProp] = value;
      }
      if (/^@media/.test(key)) {
        acc[key] = replaceProp(value, prop, newProp);
      }
      return acc;
    },
    {}
  );
}
