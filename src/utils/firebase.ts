import { Observable } from 'rxjs';
import { collectionData } from 'rxfire/firestore';
import type {
  CollectionReference,
  Query,
  DocumentData,
} from '@firebase/firestore-types';

import firebase from '../firebase';

export function getCollection$<TCollectionData>(
  collection: string,
  query?: (
    doc: CollectionReference<DocumentData> | Query<DocumentData>
  ) => CollectionReference<DocumentData> | Query<DocumentData>
): Observable<TCollectionData[]> {
  let collectionRef: CollectionReference<DocumentData> | Query<DocumentData> =
    firebase.firestore().collection(collection);
  if (query) {
    collectionRef = query(collectionRef);
  }
  return collectionData<TCollectionData>(collectionRef, 'id');
}

export async function getDocuments<TData extends { id: string }>(
  collection: string,
  query?: (
    doc: CollectionReference<DocumentData> | Query<DocumentData>
  ) => CollectionReference<DocumentData> | Query<DocumentData>
) {
  try {
    let collectionRef: CollectionReference<DocumentData> | Query<DocumentData> =
      firebase.firestore().collection(collection);
    if (query) {
      collectionRef = query(collectionRef);
    }

    const querySnapshot = await collectionRef.get();
    const result: TData[] = [];
    querySnapshot.forEach((doc) => {
      result.push({ ...doc.data(), id: doc.id } as TData);
    });
    return result;
  } catch (err) {
    return [];
  }
}

export async function getDocument<TData extends { id: string }>(
  collection: string,
  id: string
) {
  return firebase
    .firestore()
    .collection(collection)
    .doc(id)
    .get()
    .then(
      (docRef) =>
        ({
          id: docRef.id,
          ...docRef.data(),
        } as TData)
    );
}

export async function createDocument<TData>(
  collection: string,
  data: Omit<TData, 'id'>
) {
  return firebase
    .firestore()
    .collection(collection)
    .add(data)
    .then((docRef) => ({
      id: docRef.id,
      ...data,
    }));
}

export async function updateDocument<TData>(
  collection: string,
  id: string,
  data: Partial<TData>
) {
  return firebase.firestore().collection(collection).doc(id).update(data);
}

export async function deleteDocument(collection: string, id: string) {
  return firebase.firestore().collection(collection).doc(id).delete();
}
