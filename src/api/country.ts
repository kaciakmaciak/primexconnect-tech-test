export interface CountriesResponseItem {
  name: string;
}

export type CountriesResponse = CountriesResponseItem[];

export async function fetchCountries(): Promise<CountriesResponse> {
  return fetch('https://restcountries.eu/rest/v2/all').then(
    async (response) => {
      const data = await response.json();
      if (response.status >= 400) {
        throw new Error(data?.message || response.statusText);
      }
      return data;
    }
  );
}
