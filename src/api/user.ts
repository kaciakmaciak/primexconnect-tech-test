import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  createDocument,
  deleteDocument,
  getCollection$,
  getDocument,
  getDocuments,
  updateDocument,
} from '../utils/firebase';

import type { User } from '../types/user';
import type { UserRole } from '../types/user-role';
import {
  createOrganization,
  OrganizationDocument,
  updateOrganization,
} from './organization';

export interface OrganizationFeatureDocument {
  id: string;
  name: string;
}

export interface UserRoleDocument {
  id: string;
  name: string;
}

export function getUserRoles$(): Observable<UserRole[]> {
  return getCollection$<UserRoleDocument>('user_roles').pipe(
    map((roles) =>
      roles.map((role) => ({
        id: role.id,
        name: role.name,
      }))
    )
  );
}

export interface UserDocument {
  id: string;
  name: string;
  email: string;
  country: string;
  roleId: string;
  organizationId: string;
}

export function getUsers$(
  organizationId?: string,
  limit?: number,
  startAt?: number
): Observable<User[]> {
  return combineLatest([
    getCollection$<UserDocument>('users', (doc) => {
      let newDoc = doc;
      if (organizationId) {
        newDoc = newDoc.where('organizationId', '==', organizationId);
      }
      if (limit) {
        newDoc = newDoc.limit(limit);
      }
      if (startAt) {
        newDoc = newDoc.startAt(startAt);
      }
      return newDoc;
    }),
    getUserRoles$(),
  ]).pipe(
    map(([users, userRoles]) =>
      users.map((user) => ({
        id: user.id,
        name: user.name,
        email: user.email,
        country: user.country,
        role: userRoles.find((role) => role.id === user.roleId) || null,
        organizationId: user.organizationId,
      }))
    )
  );
}

export interface CreateOrUpdateUserRequest {
  name: string;
  email: string;
  roleId: string;
  organizationName: string;
  organizationFeaturesIds: string[];
  country: string;
}

export async function getUser(id: string) {
  return getDocument<UserDocument>('users', id);
}

/**
 * Creates user and returns newly created user document.
 */
export async function createUser(data: CreateOrUpdateUserRequest) {
  const organizations = await getDocuments<OrganizationDocument>(
    'organizations',
    (doc) => doc.where('name', '==', data.organizationName)
  );
  let organizationId: string;
  if (organizations.length === 0) {
    const newOrganization = await createOrganization({
      name: data.organizationName,
      featuresIds: data.organizationFeaturesIds,
    });
    organizationId = newOrganization.id;
  } else {
    organizationId = organizations[0].id;
    await updateOrganization(organizationId, {
      featuresIds: data.organizationFeaturesIds,
    });
  }
  return createDocument<UserDocument>('users', {
    name: data.name,
    email: data.email,
    roleId: data.roleId,
    organizationId,
    country: data.country,
  });
}

/**
 * Updates user.
 */
export async function updateUser(id: string, data: CreateOrUpdateUserRequest) {
  const organizations = await getDocuments<OrganizationDocument>(
    'organizations',
    (doc) => doc.where('name', '==', data.organizationName)
  );
  let organizationId: string;
  if (organizations.length === 0) {
    const newOrganization = await createOrganization({
      name: data.organizationName,
      featuresIds: data.organizationFeaturesIds,
    });
    organizationId = newOrganization.id;
  } else {
    organizationId = organizations[0].id;
    await updateOrganization(organizationId, {
      featuresIds: data.organizationFeaturesIds,
    });
  }
  return updateDocument<Omit<UserDocument, 'id'>>('users', id, {
    name: data.name,
    email: data.email,
    roleId: data.roleId,
    organizationId,
    country: data.country,
  });
}

export async function deleteUser(userId: string) {
  return deleteDocument('users', userId);
}
