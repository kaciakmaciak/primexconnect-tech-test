import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  createDocument,
  getCollection$,
  updateDocument,
} from '../utils/firebase';

import type { Organization } from '../types/organization';
import type { OrganizationFeature } from '../types/organization-feature';

export interface OrganizationFeatureDocument {
  id: string;
  name: string;
}

export function getOrganizationFeatures$(): Observable<OrganizationFeature[]> {
  return getCollection$<OrganizationFeatureDocument>(
    'organization_features'
  ).pipe(
    map((features) =>
      features.map((feature) => ({
        id: feature.id,
        name: feature.name,
      }))
    )
  );
}

export interface OrganizationDocument {
  id: string;
  name: string;
  featuresIds: string[];
}

export function getOrganizations$(): Observable<Organization[]> {
  return combineLatest([
    getCollection$<OrganizationDocument>('organizations', (doc) =>
      doc.orderBy('name', 'asc')
    ),
    getOrganizationFeatures$(),
  ]).pipe(
    map(([organizations, features]) =>
      organizations.map((organization) => ({
        id: organization.id,
        name: organization.name,
        features: organization.featuresIds
          .map((featureId) =>
            features.find((feature) => feature.id === featureId)
          )
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          .filter<OrganizationFeature>(Boolean as any),
      }))
    )
  );
}

export type CreateOrganizationRequest = Omit<OrganizationDocument, 'id'>;

/**
 * Creates organization and returns its id.
 */
export async function createOrganization(data: CreateOrganizationRequest) {
  return createDocument<Omit<OrganizationDocument, 'id'>>(
    'organizations',
    data
  );
}

/**
 * Updates organization.
 */
export async function updateOrganization(
  id: string,
  data: Partial<OrganizationDocument>
) {
  return updateDocument<Omit<OrganizationDocument, 'id'>>(
    'organizations',
    id,
    data
  );
}
