import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { PageLayout } from './layouts/PageLayout';

import { AppHeader } from './components/AppHeader';
import { Sidebar } from './components/Sidebar';

import { WorkspaceLayout } from './layouts/WorkspaceLayout';
import { UsersListHeader } from './routes/organization/UsersListHeader';
import { UserActionHeader } from './routes/user/UserActionHeader';
import { WorkspaceFooter } from './components/WorkspaceFooter';

import { UsersList } from './routes/organization/UsersList';
import { CreateUser } from './routes/user/CreateUser';
import { EditUser } from './routes/user/EditUser';

function App() {
  return (
    <PageLayout
      title="PrimeXConnect Tech Test"
      Header={AppHeader}
      Sidebar={Sidebar}
    >
      <Switch>
        <Route exact path={['/', '/organization/:organizationId']}>
          <WorkspaceLayout Header={UsersListHeader} Footer={WorkspaceFooter}>
            <UsersList />
          </WorkspaceLayout>
        </Route>
        <Route path="/user/create" exact>
          <WorkspaceLayout Header={UserActionHeader} Footer={WorkspaceFooter}>
            <CreateUser />
          </WorkspaceLayout>
        </Route>
        <Route path="/user/:userId/edit" exact>
          <WorkspaceLayout Header={UserActionHeader} Footer={WorkspaceFooter}>
            <EditUser />
          </WorkspaceLayout>
        </Route>
      </Switch>
    </PageLayout>
  );
}

export default App;
