import { Organization } from './organization';
import { UserRole } from './user-role';

export interface User {
  id: string;
  name: string;
  email: string;
  country: string;
  role: UserRole | null;
  organizationId: string;
  organization?: Organization;
}
