import { OrganizationFeature } from './organization-feature';

export interface Organization {
  id: string;
  name: string;
  features: OrganizationFeature[];
}
