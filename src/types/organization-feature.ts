export interface OrganizationFeature {
  id: string;
  name: string;
}
