import { useCallback, useEffect, useMemo, useRef } from 'react';
import {
  QueryKey,
  useQuery,
  useQueryClient,
  UseQueryOptions,
  UseQueryResult,
} from 'react-query';
import { Observable, Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

export function useSubscription<
  TSubscriptionFnData = unknown,
  TError = Error,
  TData = TSubscriptionFnData,
  TSubscriptionKey extends QueryKey = QueryKey
>(
  subscriptionKey: TSubscriptionKey,
  stream$: Observable<TSubscriptionFnData>,
  options: Omit<
    UseQueryOptions<TSubscriptionFnData, TError, TData, TSubscriptionKey>,
    'select'
  > = {}
) {
  const queryClient = useQueryClient();

  const subscription = useRef<Subscription>();

  useEffect(() => {
    return () => {
      subscription.current?.unsubscribe();
    };
  }, []);

  return useQuery<TSubscriptionFnData, TError, TData, TSubscriptionKey>(
    subscriptionKey,
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    () =>
      new Promise((resolve, reject) => {
        subscription.current?.unsubscribe();
        subscription.current = stream$
          .pipe(
            catchError((error) => {
              reject(error);
              return throwError(() => error);
            })
          )
          .subscribe((data) => {
            resolve(data);
            queryClient.setQueryData(subscriptionKey, data);
          });
      }),
    {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      ...options,
    }
  );
}

interface UseCombineQueryResultsReturn<TData> {
  isLoading: boolean;
  error: Error | null;
  data: TData | null;
}

export function useCombineQueryResults<TData, TQueryData extends unknown[]>(
  queryResults: UseQueryResult<unknown, Error>[],
  combineFn: (data: TQueryData) => TData | null
): UseCombineQueryResultsReturn<TData> {
  const isLoading = queryResults.some((queryResult) => queryResult.isLoading);
  const error =
    queryResults.find((queryResult) => queryResult.error)?.error || null;
  const data = queryResults.map((queryResult) => queryResult.data);

  const dataFn = useCallback(
    (data: TQueryData) => {
      if (isLoading || error) {
        return null;
      }
      return combineFn(data);
    },
    [combineFn, error, isLoading]
  );

  return useMemo(
    () => ({
      isLoading,
      error,
      data: dataFn(data as TQueryData),
    }),
    [data, isLoading, error, dataFn]
  );
}
