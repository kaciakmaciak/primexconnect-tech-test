import { useMediaQuery, useTheme } from '@material-ui/core';

export function useIsDesktop() {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.up('sm'));
}
