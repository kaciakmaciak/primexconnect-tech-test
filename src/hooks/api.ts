import { useCallback, useMemo } from 'react';
import {
  UseMutationOptions,
  UseQueryOptions,
  useMutation,
  useQueryClient,
  useQuery,
} from 'react-query';

import {
  getOrganizationFeatures$,
  getOrganizations$,
} from '../api/organization';
import {
  getUsers$,
  getUserRoles$,
  createUser,
  CreateOrUpdateUserRequest,
  UserDocument,
  updateUser,
  getUser,
  deleteUser,
} from '../api/user';

import { useCombineQueryResults, useSubscription } from '../hooks/react-query';

import type { Organization } from '../types/organization';
import type { OrganizationFeature } from '../types/organization-feature';
import type { User } from '../types/user';
import type { UserRole } from '../types/user-role';

const organizationFeatures$ = getOrganizationFeatures$();

export function useOrganizationFeaturesSubscription<
  Data = OrganizationFeature[]
>(options: UseQueryOptions<OrganizationFeature[], Error, Data> = {}) {
  return useSubscription(
    'organizationFeatures',
    organizationFeatures$,
    options
  );
}

const organizations$ = getOrganizations$();

export function useOrganizationsSubscription<Data = Organization[]>(
  options: UseQueryOptions<Organization[], Error, Data> = {}
) {
  return useSubscription('organizations', organizations$, options);
}

export function useOrganizationSubscription(
  id: string,
  options: UseQueryOptions<Organization[], Error, Organization | null> = {}
) {
  return useOrganizationsSubscription<Organization | null>({
    ...options,
    select: (organizations) =>
      organizations.find((organization) => organization.id === id) || null,
  });
}

const userRoles$ = getUserRoles$();

export function useUserRolesSubscription<TData = UserRole[]>(
  options: UseQueryOptions<UserRole[], Error, TData> = {}
) {
  return useSubscription<UserRole[], Error, TData>(
    'userRoles',
    userRoles$,
    options
  );
}

export function useUsersSubscription<TData = User[]>(
  organizationId: string,
  limit?: number,
  startAt?: number,
  options?: UseQueryOptions<User[], Error, TData>
) {
  return useSubscription<User[], Error, TData>(
    ['users', organizationId || 'all', limit || -1, startAt || 0],
    useMemo(
      () => getUsers$(organizationId, limit, startAt),
      [limit, organizationId, startAt]
    ),
    options
  );
}

export function useUsersDataSubscription(
  organizationId: string,
  page?: number,
  pageSize?: number
) {
  const combineFn = useCallback(
    ([users, organizations]: [User[], Organization[]]) => {
      return users.map((user) => ({
        ...user,
        organization: organizations.find(
          (organization) => organization.id === user.organizationId
        ),
      }));
    },
    []
  );

  const startAt =
    typeof pageSize !== 'undefined' && typeof page !== 'undefined'
      ? pageSize * page
      : undefined;

  return useCombineQueryResults<User[], [User[], Organization[]]>(
    [
      useUsersSubscription(organizationId, pageSize, startAt),
      useOrganizationsSubscription(),
    ],
    combineFn
  );
}

export function useUserDataQuery<TData>(
  userId: string,
  options?: UseQueryOptions<UserDocument, Error, TData>
) {
  useQuery(['user', userId], () => getUser(userId), options);
}

export function useCreateUserMutation(
  options?: UseMutationOptions<UserDocument, Error, CreateOrUpdateUserRequest>
) {
  const queryClient = useQueryClient();
  return useMutation((data: CreateOrUpdateUserRequest) => createUser(data), {
    ...(options || {}),
    onSuccess: async (data, variables, ctx) => {
      await Promise.all([
        queryClient.refetchQueries('organizations'),
        queryClient.refetchQueries(['users', 'all']),
        queryClient.refetchQueries(['users', data.organizationId]),
      ]);
      if (options?.onSuccess) {
        await options.onSuccess(data, variables, ctx);
      }
    },
  });
}

export function useUpdateUserMutation(
  id: string,
  options?: UseMutationOptions<void, Error, CreateOrUpdateUserRequest>
) {
  const queryClient = useQueryClient();
  return useMutation(
    (data: CreateOrUpdateUserRequest) => updateUser(id, data),
    {
      ...(options || {}),
      onSuccess: async (data, variables, ctx) => {
        await queryClient.refetchQueries();
        if (options?.onSuccess) {
          await options.onSuccess(data, variables, ctx);
        }
      },
    }
  );
}

export function useDeleteUserMutation(
  options?: UseMutationOptions<void, Error, string>
) {
  return useMutation((id: string) => deleteUser(id), options);
}
