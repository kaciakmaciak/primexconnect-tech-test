import { useQuery, UseQueryOptions } from 'react-query';

import * as countryApi from '../api/country';

export const countriesQueryKey = 'countries';

export function useCountriesQuery<T = countryApi.CountriesResponse>(
  options?: UseQueryOptions<countryApi.CountriesResponse, Error, T>
) {
  return useQuery(
    countriesQueryKey,
    () => countryApi.fetchCountries(),
    options
  );
}

export function useCountryNamesQuery(
  options?: UseQueryOptions<countryApi.CountriesResponse, Error, string[]>
) {
  return useCountriesQuery<string[]>({
    ...options,
    select: (countries) => countries.map((country) => country.name),
  });
}
