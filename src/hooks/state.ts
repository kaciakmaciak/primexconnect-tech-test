import { useState, useCallback } from 'react';

export function useToggle(defaultOn = false): [boolean, () => void] {
  const [on, setOn] = useState<boolean>(defaultOn);
  const toggle = useCallback(() => {
    setOn((on) => !on);
  }, []);
  return [on, toggle];
}
