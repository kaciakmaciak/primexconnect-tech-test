import React from 'react';
import { Control, Path, useController } from 'react-hook-form';
import { TextField as MuiTextField, TextFieldProps } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';

export type Option = string;

export type Props<TFieldValues> = Omit<TextFieldProps, 'select'> & {
  control: Control<TFieldValues>;
  name: Path<TFieldValues>;
  options: Option[];
};

export function FormAutocompleteField<TFieldValues = Record<string, unknown>>(
  props: Props<TFieldValues>
) {
  const { control, name, options, ...rest } = props;
  const { field, fieldState } = useController<TFieldValues>({ name, control });
  return (
    <Autocomplete
      freeSolo
      options={options}
      onChange={(_, value) => field.onChange(value)}
      value={field.value as string}
      renderInput={(params) => (
        <MuiTextField
          {...rest}
          {...field}
          {...params}
          error={Boolean(fieldState.error?.message)}
          helperText={fieldState.error?.message || rest.helperText}
        />
      )}
    />
  );
}
