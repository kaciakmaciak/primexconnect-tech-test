import React from 'react';
import { Control, Path, useController } from 'react-hook-form';
import { MenuItem, TextField, TextFieldProps } from '@material-ui/core';

export type Option =
  | {
      value: string;
      label: string;
    }
  | string;

export type Props<TFieldValues = Record<string, unknown>> = Omit<
  TextFieldProps,
  'select'
> & {
  control: Control<TFieldValues>;
  name: Path<TFieldValues>;
  options: Option[];
  multiple?: boolean;
};

function getValueAndLabel(item: Option) {
  if (typeof item === 'string') {
    return { value: item, label: item };
  }
  return item;
}

export function FormSelectField<TFieldValues = Record<string, unknown>>(
  props: Props<TFieldValues>
) {
  const { control, name, options, multiple, ...rest } = props;
  const { field, fieldState } = useController<TFieldValues>({ name, control });
  return (
    <TextField
      SelectProps={{ multiple }}
      {...rest}
      {...field}
      select
      error={Boolean(fieldState.error?.message)}
      helperText={fieldState.error?.message || rest.helperText}
    >
      {options.map((item) => {
        const { value, label } = getValueAndLabel(item);
        return (
          <MenuItem key={value} value={value}>
            {label}
          </MenuItem>
        );
      })}
    </TextField>
  );
}
