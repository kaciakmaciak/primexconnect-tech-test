import React from 'react';
import { Control, Path, useController } from 'react-hook-form';
import { TextField, TextFieldProps } from '@material-ui/core';

export type Props<TFieldValues> = Omit<TextFieldProps, 'select'> & {
  control: Control<TFieldValues>;
  name: Path<TFieldValues>;
};

export function FormInputField<TFieldValues = Record<string, unknown>>(
  props: Props<TFieldValues>
) {
  const { control, name, ...rest } = props;
  const { field, fieldState } = useController<TFieldValues>({ name, control });
  return (
    <TextField
      {...rest}
      InputProps={field}
      error={Boolean(fieldState.error?.message)}
      helperText={fieldState.error?.message || rest.helperText}
    />
  );
}
