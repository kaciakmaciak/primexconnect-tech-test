import React from 'react';
import { Typography } from '@material-ui/core';

export interface Props {
  title: string;
}

export function AppHeader(props: Props) {
  const { title } = props;
  return <Typography noWrap>{title}</Typography>;
}
