import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { MenuItem } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

const useStyles = makeStyles(() => ({
  skeleton: {
    width: '100%',
  },
}));

export function SidebarSkeleton() {
  const classes = useStyles();
  return (
    <>
      <MenuItem>
        <Skeleton
          variant="text"
          animation="wave"
          className={classes.skeleton}
        />
      </MenuItem>
      <MenuItem>
        <Skeleton
          variant="text"
          animation="wave"
          className={classes.skeleton}
        />
      </MenuItem>
    </>
  );
}
