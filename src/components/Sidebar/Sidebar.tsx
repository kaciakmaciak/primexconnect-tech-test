import React from 'react';
import { List } from '@material-ui/core';

import { MenuLink } from '../MenuLink';

import { SidebarSkeleton } from './Skeleton';

import { useOrganizationsSubscription } from '../../hooks/api';

export function Sidebar() {
  const {
    data: organizations,
    isLoading,
    error,
  } = useOrganizationsSubscription();

  return (
    <List>
      <MenuLink to={`/`} exact>
        All Users
      </MenuLink>
      {isLoading && <SidebarSkeleton />}
      {!isLoading &&
        !error &&
        organizations &&
        organizations.map((organization) => (
          <React.Fragment key={organization.id}>
            <MenuLink to={`/organization/${organization.id}`}>
              {organization.name}
            </MenuLink>
          </React.Fragment>
        ))}
    </List>
  );
}
