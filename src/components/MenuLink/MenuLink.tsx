import React from 'react';
import { useRouteMatch, Link } from 'react-router-dom';
import { MenuItem } from '@material-ui/core';

export interface Props {
  to: string;
  exact?: boolean;
}

export function MenuLink(props: React.PropsWithChildren<Props>) {
  const { to, exact, children } = props;
  const match = useRouteMatch({
    path: to,
    exact,
  });

  return (
    <MenuItem selected={Boolean(match)} component={Link} to={to}>
      {children}
    </MenuItem>
  );
}
