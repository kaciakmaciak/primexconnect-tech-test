import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Drawer, IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import { useToggle } from '../../hooks/state';
import { useIsDesktop } from '../../hooks/media';

export interface Props {
  title: string;
  Header: React.ComponentType<{ title: string }>;
  Sidebar: React.ComponentType<Record<string, never>>;
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100vw',
    height: '100vh',
    display: 'grid',
    gridTemplateAreas: '"header" "content"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: 'auto 1fr',
    [theme.breakpoints.up('sm')]: {
      gridTemplateColumns: `${drawerWidth}px auto`,
      gridTemplateAreas: '"header header" "sidebar content"',
    },
  },
  appBar: {
    gridArea: 'header',
    zIndex: theme.zIndex.drawer + 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  sidebar: {
    gridArea: 'sidebar',
  },
  drawerDocked: {
    width: drawerWidth,
    height: '100%',
    flexShrink: 0,
  },
  drawerPaperAnchorDocked: {
    position: 'static',
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    gridArea: 'content',
    flexGrow: 1,
    overflowY: 'auto',
  },
}));

export function PageLayout(props: React.PropsWithChildren<Props>) {
  const { title, Header, Sidebar, children } = props;

  const isDesktop = useIsDesktop();
  const [mobileMenuOpen, toggleMenuOpen] = useToggle(false);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={toggleMenuOpen}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Header title={title} />
        </Toolbar>
      </AppBar>
      <nav className={classes.sidebar}>
        <Drawer
          variant={isDesktop ? 'permanent' : 'temporary'}
          anchor="left"
          open={mobileMenuOpen}
          onClose={toggleMenuOpen}
          classes={{
            paper: classes.drawerPaper,
            docked: classes.drawerDocked,
            paperAnchorDockedLeft: classes.drawerPaperAnchorDocked,
            paperAnchorDockedRight: classes.drawerPaperAnchorDocked,
          }}
          ModalProps={{
            keepMounted: true,
          }}
        >
          <div className={classes.drawerContainer}>
            <Sidebar />
          </div>
        </Drawer>
      </nav>
      <main className={classes.content}>{children}</main>
    </div>
  );
}
