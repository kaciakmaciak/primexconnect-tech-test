import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Toolbar } from '@material-ui/core';

export interface Props {
  Header: React.ComponentType<Record<string, never>>;
  Footer: React.ComponentType<Record<string, never>>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    height: '100%',
  },
  headerContainer: {
    position: 'sticky',
    top: 0,
  },
  header: {
    backgroundColor: theme.palette.background.paper,
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  footerContainer: {
    position: 'sticky',
    bottom: 0,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    borderTop: `1px solid ${theme.palette.divider}`,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(3),
    },
    overflowY: 'auto',
  },
}));

export function WorkspaceLayout(props: React.PropsWithChildren<Props>) {
  const { Header, Footer, children } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.headerContainer}>
        <Toolbar className={classes.header}>
          <Header />
        </Toolbar>
      </div>
      <div className={classes.content}>{children}</div>
      <div className={classes.footerContainer}>
        <Toolbar className={classes.footer}>
          <Footer />
        </Toolbar>
      </div>
    </div>
  );
}
