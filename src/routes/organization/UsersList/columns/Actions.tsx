import React from 'react';
import { Link } from 'react-router-dom';
import { IconButton } from '@material-ui/core';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDeleteUserMutation } from '../../../../hooks/api';

interface Props {
  userId: string | undefined;
}

export function ActionsColumn(props: Props) {
  const { mutate, isLoading: isDeleting } = useDeleteUserMutation();

  const handleDelete = () => {
    if (props.userId) {
      mutate(props.userId);
    }
  };

  if (!props.userId) return null;

  return (
    <>
      <IconButton
        size="small"
        component={Link}
        to={`/user/${props.userId}/edit`}
      >
        <EditIcon />
      </IconButton>
      <IconButton disabled={isDeleting} size="small" onClick={handleDelete}>
        <DeleteIcon />
      </IconButton>
    </>
  );
}
