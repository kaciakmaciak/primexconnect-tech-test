import React from 'react';
import { Chip } from '@material-ui/core';

import type { OrganizationFeature } from '../../../../types/organization-feature';

interface Props {
  features: OrganizationFeature[];
}

export function OrganizationFeaturesColumn(props: Props) {
  return (
    <div>
      {props.features.map((feature) => (
        <React.Fragment key={feature.id}>
          <Chip label={feature.name} size="small" />{' '}
        </React.Fragment>
      ))}
    </div>
  );
}
