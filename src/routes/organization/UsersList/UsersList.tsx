import React, { useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { DataGrid } from '@material-ui/data-grid';
import type { GridColDef } from '@material-ui/data-grid';

import { useUsersDataSubscription } from '../../../hooks/api';
import { useIsDesktop } from '../../../hooks/media';

import { OrganizationFeaturesColumn } from './columns/Features';
import { ActionsColumn } from './columns/Actions';

import type { UserRole } from '../../../types/user-role';
import type { Organization } from '../../../types/organization';

interface UsersListMatcher {
  organizationId?: string;
}

export function UsersList() {
  const { organizationId } = useParams<UsersListMatcher>();

  const isDesktop = useIsDesktop();

  const {
    data: users,
    isLoading: isUsersLoading,
    error: userError,
  } = useUsersDataSubscription(organizationId || '');

  const columns: GridColDef[] = useMemo(
    () => [
      { field: 'name', headerName: 'Name', flex: isDesktop ? 1 : undefined },
      { field: 'email', headerName: 'Email', flex: 1.5 },
      {
        field: 'role.name',
        headerName: 'Role',
        valueGetter: (params) =>
          (params.getValue(params.id, 'role') as UserRole)?.name,
      },
      {
        field: 'organization.name',
        headerName: 'Organization',
        flex: 1,
        valueGetter: (params) =>
          (params.getValue(params.id, 'organization') as Organization)?.name,
      },
      {
        field: 'organization.features.name',
        headerName: 'Organization Features',
        sortable: false,
        valueGetter: (params) =>
          (
            params.getValue(params.id, 'organization') as Organization
          )?.features.map((feature) => feature.name),
        flex: isDesktop ? 1.5 : undefined,
        width: isDesktop ? undefined : 200,
        // eslint-disable-next-line react/display-name
        renderCell: (params) => (
          <OrganizationFeaturesColumn
            features={
              (params.getValue(params.id, 'organization') as Organization)
                ?.features || []
            }
          />
        ),
      },
      { field: 'country', headerName: 'Country', width: 120 },
      {
        field: 'actions',
        headerName: ' ',
        width: 80,
        sortable: false,
        // eslint-disable-next-line react/display-name
        renderCell: (params) => (
          <ActionsColumn
            userId={(params.getValue(params.id, 'id') as string) || undefined}
          />
        ),
      },
    ],
    [isDesktop]
  );

  return (
    <DataGrid
      rows={users || []}
      columns={columns}
      loading={isUsersLoading}
      error={userError}
      disableColumnFilter
      autoHeight
      autoPageSize
    />
  );
}
