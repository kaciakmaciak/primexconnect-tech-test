import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';

import { useOrganizationSubscription } from '../../../hooks/api';

const useStyles = makeStyles(() => ({
  spacer: {
    flexGrow: 1,
  },
}));

interface UsersListMatcher {
  organizationId?: string;
}

export function UsersListHeader() {
  const { organizationId } = useParams<UsersListMatcher>();

  const { data: organization } = useOrganizationSubscription(
    organizationId || ''
  );
  const classes = useStyles();

  return (
    <>
      <Typography variant="h6">{organization?.name || 'All Users'}</Typography>
      <span className={classes.spacer} />
      <Button
        variant="outlined"
        color="primary"
        startIcon={<AddIcon />}
        component={Link}
        to="/user/create"
      >
        Create User
      </Button>
    </>
  );
}
