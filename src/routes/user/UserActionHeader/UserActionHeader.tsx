import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button } from '@material-ui/core';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const useStyles = makeStyles(() => ({
  spacer: {
    flexGrow: 1,
  },
}));

interface UserMatcher {
  userId?: string;
}

export function UserActionHeader() {
  const { userId } = useParams<UserMatcher>();

  const classes = useStyles();

  const history = useHistory();
  const handleBack = () => {
    history.goBack();
  };

  return (
    <>
      <Typography variant="h6">
        {userId ? 'Edit User' : 'Create User'}
      </Typography>
      <span className={classes.spacer} />
      <Button startIcon={<ArrowBackIcon />} onClick={handleBack}>
        Back
      </Button>
    </>
  );
}
