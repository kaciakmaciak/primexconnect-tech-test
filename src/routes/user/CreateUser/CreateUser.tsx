import React from 'react';
import { useHistory } from 'react-router';

import { UserForm } from '../UserForm';

export function CreateUser() {
  const history = useHistory();

  return <UserForm onSubmitSuccess={() => history.push('/')} />;
}
