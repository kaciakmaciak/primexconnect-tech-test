export interface FormValues {
  name: string;
  email: string;
  roleId: string;
  organizationName: string;
  organizationFeaturesIds: string[];
  country: string;
}
