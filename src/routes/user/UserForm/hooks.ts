import { useEffect } from 'react';
import { UseFormReturn } from 'react-hook-form';

import { useOrganizationsSubscription } from '../../../hooks/api';

import type { FormValues } from './types/form-values';

export function useResetOrganizationFeatures(form: UseFormReturn<FormValues>) {
  const { watch, setValue } = form;

  const organizationName = watch('organizationName');

  const { data: organizations } = useOrganizationsSubscription();

  useEffect(() => {
    if (!organizations || !organizationName) return;

    const organization = organizations.find(
      (organization) => organization.name === organizationName
    );
    const featuresIds = organization?.features?.map((feature) => feature.id);
    if (featuresIds) {
      setValue('organizationFeaturesIds', featuresIds);
    }
  }, [organizations, organizationName, setValue]);
}
