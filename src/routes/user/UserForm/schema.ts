import * as yup from 'yup';

export const userSchema = yup.object().shape({
  name: yup.string().required('Please specify the name of the user'),
  email: yup
    .string()
    .email('Please provide a valid email address')
    .required('Please provide a valid email address'),
  roleId: yup.string().required('Please select a role'),
  organizationName: yup
    .string()
    .required('Please specify the organization name'),
  country: yup.string().required('Please select a country'),
});
