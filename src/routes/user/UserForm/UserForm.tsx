import React, { useCallback } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import {
  useCreateUserMutation,
  useUpdateUserMutation,
} from '../../../hooks/api';

import { FormInputField } from '../../../components/FormInputField';

import { FormCountrySelectField } from './fields/FormCountrySelectField';
import { FormUserRoleSelectField } from './fields/FormUserRoleSelectField';
import { FormFeaturesSelectField } from './fields/FormFeaturesSelectField';
import { FormOrganizationNameField } from './fields/FormOrganizationNameField';

import type { User } from '../../../types/user';
import type { FormValues } from './types/form-values';

import { useResetOrganizationFeatures } from './hooks';

import { userSchema } from './schema';

export interface Props {
  user?: User | undefined;
  onSubmitSuccess?: () => void;
}

const useStyles = makeStyles(() => ({
  inputField: {
    width: '100%',
  },
}));

export function UserForm(props: Props) {
  const { user, onSubmitSuccess } = props;

  const {
    mutate: createUser,
    isLoading: isCreating,
    error: createError,
  } = useCreateUserMutation({
    onSuccess: onSubmitSuccess,
  });
  const {
    mutate: updateUser,
    isLoading: isUpdating,
    error: updateError,
  } = useUpdateUserMutation(user?.id || '', {
    onSuccess: onSubmitSuccess,
  });

  const form = useForm<FormValues>({
    resolver: yupResolver(userSchema),
    defaultValues: {
      name: user?.name || '',
      email: user?.email || '',
      roleId: user?.role?.id || '',
      organizationName: user?.organization?.name || '',
      organizationFeaturesIds:
        user?.organization?.features.map((feature) => feature.id) || [],
      country: user?.country || '',
    },
  });
  const { control, handleSubmit } = form;

  useResetOrganizationFeatures(form);

  const onSubmit = useCallback(
    (formValues: FormValues) => {
      if (user) {
        return updateUser(formValues);
      } else {
        return createUser(formValues);
      }
    },
    [user, createUser, updateUser]
  );

  const classes = useStyles();

  const error = createError || updateError;
  const isSubmitting = isCreating || isUpdating;

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} lg={4} xl={3}>
          <FormInputField<FormValues>
            control={control}
            name="name"
            label="Name"
            autoFocus
            className={classes.inputField}
          />
        </Grid>

        <Grid item xs={12} md={6} lg={4} xl={3}>
          <FormInputField<FormValues>
            control={control}
            name="email"
            label="Email Address"
            className={classes.inputField}
          />
        </Grid>

        <Grid item xs={12} md={6} lg={4} xl={3}>
          <FormUserRoleSelectField<FormValues>
            control={control}
            name="roleId"
            label="User Role"
            className={classes.inputField}
          />
        </Grid>

        <Grid item xs={12} md={6} lg={4} xl={3}>
          <FormOrganizationNameField<FormValues>
            control={control}
            name="organizationName"
            label="Organization Name"
            className={classes.inputField}
          />
        </Grid>

        <Grid item xs={12} md={6} lg={4} xl={3}>
          <FormFeaturesSelectField
            control={control}
            name="organizationFeaturesIds"
            label="Organization Features (Optional)"
            className={classes.inputField}
            multiple
            helperText="Updating features will have impact on all users within the organization"
          />
        </Grid>

        <Grid item xs={12} md={6} lg={4} xl={3}>
          <FormCountrySelectField<FormValues>
            control={control}
            name="country"
            label="Country"
            className={classes.inputField}
          />
        </Grid>

        {error && (
          <Grid item xs={12}>
            <Alert severity="error">{error?.message}</Alert>
          </Grid>
        )}

        <Grid item xs={12}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            disabled={isSubmitting}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
