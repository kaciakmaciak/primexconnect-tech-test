import React from 'react';

import { useOrganizationFeaturesSubscription } from '../../../../hooks/api';

import {
  FormSelectField,
  FormSelectFieldProps,
} from '../../../../components/FormSelectField';

export type Props<TFieldValues> = Omit<
  FormSelectFieldProps<TFieldValues>,
  'options'
>;

export function FormFeaturesSelectField<TFieldValues = Record<string, unknown>>(
  props: Props<TFieldValues>
) {
  const { data: features } = useOrganizationFeaturesSubscription({
    select: (features) =>
      features.map((feature) => ({
        value: feature.id,
        label: feature.name,
      })),
  });
  return <FormSelectField {...props} options={features || []} />;
}
