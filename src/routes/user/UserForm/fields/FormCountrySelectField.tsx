import React from 'react';

import { useCountryNamesQuery } from '../../../../hooks/country';

import {
  FormSelectField,
  FormSelectFieldProps,
} from '../../../../components/FormSelectField';

export type Props<TFieldValues> = Omit<
  FormSelectFieldProps<TFieldValues>,
  'options'
>;

export function FormCountrySelectField<TFieldValues = Record<string, unknown>>(
  props: Props<TFieldValues>
) {
  const { data: countries } = useCountryNamesQuery();
  return <FormSelectField {...props} options={countries || []} />;
}
