import React from 'react';

import { useOrganizationsSubscription } from '../../../../hooks/api';

import {
  FormAutocompleteField,
  FormAutocompleteFieldProps,
} from '../../../../components/FormAutocompleteField';

export type Props<TFieldValues> = Omit<
  FormAutocompleteFieldProps<TFieldValues>,
  'options'
>;

export function FormOrganizationNameField<
  TFieldValues = Record<string, unknown>
>(props: Props<TFieldValues>) {
  const { data: userRoles } = useOrganizationsSubscription({
    select: (organizations) =>
      organizations.map((organization) => organization.name),
  });
  return <FormAutocompleteField {...props} options={userRoles || []} />;
}
