import React from 'react';

import { useUserRolesSubscription } from '../../../../hooks/api';

import {
  FormSelectField,
  FormSelectFieldProps,
} from '../../../../components/FormSelectField';

export type Props<TFieldValues> = Omit<
  FormSelectFieldProps<TFieldValues>,
  'options'
>;

export function FormUserRoleSelectField<TFieldValues = Record<string, unknown>>(
  props: Props<TFieldValues>
) {
  const { data: userRoles } = useUserRolesSubscription({
    select: (userRoles) =>
      userRoles.map((userRole) => ({
        value: userRole.id,
        label: userRole.name,
      })),
  });
  return <FormSelectField {...props} options={userRoles || []} />;
}
