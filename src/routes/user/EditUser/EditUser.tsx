import React, { useMemo } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { useUsersDataSubscription } from '../../../hooks/api';

import { UserForm } from '../UserForm';

interface UserMatcher {
  userId?: string;
}

export function EditUser() {
  const { userId } = useParams<UserMatcher>();

  const history = useHistory();

  const { data: users, isLoading } = useUsersDataSubscription('');
  const user = useMemo(
    () => (users || []).find((user) => user.id === userId),
    [userId, users]
  );

  if (isLoading) return null;

  if (!user) {
    return <>User Not Found</>;
  }

  return <UserForm user={user} onSubmitSuccess={() => history.push('/')} />;
}
